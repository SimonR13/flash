package fp.lib.math
{
	public final class MathHelper
	{		
		public static function random(low:int,high:int): int
		{
			return low + Math.floor(Math.random() * (high - low + 1));
		}
		
		public function MathHelper()
		{
			throw new ArgumentError("MathHelper is a static utility class.");
		}
	}
}
package fp.examples.unit4
{
	import flash.display.*;
	import flash.events.*;
	import flash.net.URLRequest;
	
	import fp.EntryPoint;

	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class LoaderInfoDemo extends EntryPoint
	{
		public function LoaderInfoDemo()
		{			
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
			loader.load(new URLRequest("http://upload.wikimedia.org/wikipedia/commons/5/52/Rhea_hi-res_PIA07763.jpg"));
		}
		
		private function onProgress(event:ProgressEvent): void
		{
			console.clear();
			console.print("Fortschritt: " + Math.round((event.bytesLoaded / event.bytesTotal) * 100) + "%");
		}
		
		private function onComplete(event:Event): void
		{	
			console.clear();
			
			var loader:Loader = event.target.loader as Loader;
			
			var scaleFactorX:Number =  width / loader.width;
			var scaleFactorY:Number = height / loader.height; 
			var scaleFactor:Number = Math.min(scaleFactorX, scaleFactorY);
			loader.scaleX = loader.scaleY = scaleFactor;
	
			loader.x = (stage.stageWidth - loader.width) / 2;
			loader.y = (stage.stageHeight - loader.height) / 2;
			addChild(loader);
			
			loader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, onProgress);
			loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onComplete);
		}
	}
}
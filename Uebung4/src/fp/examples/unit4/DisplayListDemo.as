package fp.examples.unit4
{
	import flash.display.Sprite;
	
	import fp.lib.text.*;

	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class DisplayListDemo extends Sprite
	{
		public function DisplayListDemo()
		{			
			var console1:Console = new Console(this, 400, 600);
			var console2:Console = new Console(this, 400, 600);
			console2.x = console1.width;
			console2.alpha = 0.1;
			
			console1.print("Console 1:");
			console1.print(Text.LOREM_IPSUM);
			console2.print("Console 2:");
			console2.print(Text.LOREM_IPSUM);
		}
	}
}
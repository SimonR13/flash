package fp.examples.unit4
{
	import flash.display.*;
	import flash.events.*;
	import flash.net.URLRequest;
	
	import fp.EntryPoint;

	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class ImageDemo extends Sprite
	{
		public function ImageDemo()
		{			
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
			loader.load(new URLRequest("assets/spongebob.jpg"));
			//loader.load(new URLRequest("http://www.heise.de/icons/ho/heise_online_logo.gif"));
		}
		
		private function onComplete(event:Event): void
		{	
			var loader:Loader = event.target.loader as Loader;
			loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onComplete);
			loader.x = (stage.stageWidth - loader.width) / 2;
			loader.y = (stage.stageHeight - loader.height) / 2;
			addChild(loader);		
		}
	}
}
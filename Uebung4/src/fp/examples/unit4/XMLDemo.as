package fp.examples.unit4
{
	import flash.events.*;
	import flash.net.*;
	
	import fp.EntryPoint;
	
	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class XMLDemo extends EntryPoint
	{
		public function XMLDemo()
		{			
			var file:URLLoader = new URLLoader();
			file.addEventListener(IOErrorEvent.IO_ERROR, onError);
			file.addEventListener(Event.COMPLETE, onComplete);
			file.load(new URLRequest("assets/users.xml"));
		}
		
		private function onComplete(event:Event): void
		{
			event.target.removeEventListener(IOErrorEvent.IO_ERROR, onError);
			event.target.removeEventListener(Event.COMPLETE, onComplete);
			
			var users:XML = new XML(event.target.data);
			console.print(users);
			//console.print(users.user);
			//console.print(users.user.email);
			//console.print(users..password);
			//console.print(users.user[1].email);
			//console.print(users.user.(name=="John Doe"));
			//console.print(users.user[0].@id);
			//console.print(users.user.(@status=="active"));
			//console.print(users.user.*);
			
			/*for each (var user:* in users.user)
			{
				console.print(user.name + " (" + user.email + ")");
			}*/
		}
		
		private function onError(event:IOErrorEvent): void
		{
			event.target.removeEventListener(IOErrorEvent.IO_ERROR, onError);
			event.target.removeEventListener(Event.COMPLETE, onComplete);
			
			console.print(event.toString());
		}
	}
}
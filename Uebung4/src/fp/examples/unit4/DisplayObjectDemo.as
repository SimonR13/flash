package fp.examples.unit4
{
	import flash.display.*;
	import flash.events.*;
	import flash.net.URLRequest;
	
	import fp.EntryPoint;

	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class DisplayObjectDemo extends Sprite
	{
		public function DisplayObjectDemo()
		{			
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
			loader.load(new URLRequest("assets/Circle.swf"));
		}
		
		private function onComplete(event:Event): void
		{	
			var loader:Loader = event.target.loader as Loader;
			addChild(loader);
			loader.x = (stage.stageWidth - loader.width) / 2;
			loader.y = (stage.stageHeight - loader.height) / 2;
			
			loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onComplete);
		}
	}
}
package fp.examples.unit4
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.net.URLRequest;
	
	public class SoundDemo extends Sprite
	{		
		public function SoundDemo()
		{
			var sound:Sound = new Sound();
			sound.addEventListener(Event.COMPLETE, onComplete);
			sound.load(new URLRequest("assets/siren.mp3"));
		}
		
		private function onComplete(event:Event): void
		{
			event.target.removeEventListener(Event.COMPLETE, onComplete);
			event.target.play(0, 3);
		}
	}
}
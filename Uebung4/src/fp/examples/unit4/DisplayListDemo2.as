package fp.examples.unit4
{
	import flash.display.*;
	import flash.events.MouseEvent;
	
	import fp.lib.display.BasicShapes;
	import fp.lib.text.Console;

	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class DisplayListDemo2 extends Sprite
	{
		private var console:Console;
		private var hitTestPartner:DisplayObject;
		
		public function DisplayListDemo2() 
		{			
			console = new Console(this, 300, 330);
			console.y = 270;
			
			for (var i:int = 0; i < 10; i++)
			{
				var rect:Sprite = BasicShapes.getRectangle(100, 100, 0xff0000, 1, 2);
				rect.x = 10 + i * 75;
				rect.y = 10 + i * 50;
				rect.alpha = 0.9;
				rect.addEventListener(MouseEvent.ROLL_OVER, onRollOverRect);
				rect.addEventListener(MouseEvent.CLICK, onClickRect);
				addChild(rect);	
			}	
		}
		
		private function onRollOverRect(event:MouseEvent): void
		{
			addChild(event.target as DisplayObject);
		}
		
		private function onClickRect(event:MouseEvent): void
		{
			if (hitTestPartner != null)
			{
				console.print(hitTestPartner.hitTestObject(event.target as DisplayObject));
				hitTestPartner = null;
			}
			else if (event.shiftKey)
			{
				removeChild(event.target as DisplayObject);
			}
			else if (event.altKey)
			{
				hitTestPartner = event.target as DisplayObject;
			}
		}
	}	
}
package fp.examples.unit4
{
	import flash.events.Event;
	import flash.net.*;
	
	import fp.EntryPoint;

	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class TextDemo extends EntryPoint
	{
		public function TextDemo()
		{			
			var file:URLLoader = new URLLoader();
			file.addEventListener(Event.COMPLETE, onComplete);
			file.load(new URLRequest("assets/loremipsum.txt"));
			//file.load(new URLRequest("http://mobil.zeit.de"));
		}
		
		private function onComplete(event:Event): void
		{
			event.target.removeEventListener(Event.COMPLETE, onComplete);
			console.print(event.target.data);
		}
	}
}
package fp.examples.unit4
{
	import flash.events.Event;
	import flash.net.*;
	
	import fp.EntryPoint;
	
	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class TextVarsDemo extends EntryPoint
	{
		public function TextVarsDemo()
		{			
			var file:URLLoader = new URLLoader();
			file.dataFormat = URLLoaderDataFormat.VARIABLES;
			file.addEventListener(Event.COMPLETE, onComplete);
			
			file.load(new URLRequest("assets/variables.txt"));
		}
		
		private function onComplete(event:Event): void
		{
			event.target.removeEventListener(Event.COMPLETE, onComplete);
			
			var vars:URLVariables = event.target.data;
			for (var name:* in vars)
			{
				console.print(name + ": " + vars[name]);
			}
		}
	}
}
package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author sr
	 */
	public class Main extends Sprite 
	{
		public function Main():void 
		{
			var retArray:Object = averageAndDeviation(6, 4, 8, 2);
			trace("averageValue: " + retArray.average);
			trace("Standardabweichung: " + retArray.standardAbweichung);
		}
		
		private static function averageAndDeviation(... args): Object
		{
			var avgVal:Number = 0;
			var stdAbwe:Number = 0;
			
			if (args.length == 0) {
				return [0, 0];
			}
			else if (args.length < 2)
			{
				return [args[0], 0];
			}
			
			for (var i:uint = 0; i < args.length; i++) {
				avgVal += args[i];
			}
			avgVal = (avgVal / args.length);
			
			for (var j:uint = 0; j < args.length; j++) {
				stdAbwe += Math.pow((args[j] - avgVal), 2);
			}
			
			stdAbwe = Math.sqrt(stdAbwe / args.length);
			var retVal:Object =  { average:new Number(avgVal), standardAbweichung:new Number(stdAbwe) };
			return retVal;
		}
	}
	
}
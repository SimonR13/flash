package  
{
	/**
	 * ...
	 * @author sr
	 */
	public class MinMax 
	{
		
		private var first:Number;
		private var second:Number;
		private var third:Number;
		
		public function MinMax() 
		{
			this.first = 1.0;
			this.second = 2.0;
			this.third = 3.0;
		}
		
		public function get minimalValue(): Number
		{
			return Math.min(this.first, this.second, this.third);	
		}
		
		public function set minimalValue(newVal:Number): void
		{
			this.first = (this.first < newVal) ? newVal : this.first;
			this.second = (this.second < newVal) ? newVal : this.second;
			this.third = (this.third < newVal) ? newVal : this.third;
		}
		
		public function get maximalValue(): Number
		{
			return Math.max(this.first, this.second, this.third);
		}
		public function set maximalValue(newVal:Number): void
		{
			this.first = (this.first > newVal) ? newVal : this.first;
			this.second = (this.second > newVal) ? newVal : this.second;
			this.third = (this.third > newVal) ? newVal : this.third;			
		}
		
	}

}
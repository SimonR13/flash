package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import MinMax;
	
	/**
	 * ...
	 * @author sr
	 */
	public class Main extends Sprite 
	{
		private var test:MinMax = new MinMax();
		
		public function Main():void 
		{
			trace("MIN: ", test.minimalValue);
			trace("MAX: ", test.maximalValue);
			trace("----------------------");
			test.minimalValue = 2.0;
			trace("MIN: ", test.minimalValue);
			trace("MAX: ", test.maximalValue);
			trace("----------------------");
			test.minimalValue = 2.0;
			trace("MIN: ", test.minimalValue);
			trace("MAX: ", test.maximalValue);
			trace("----------------------");
		}
		
	}
	
}
package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author sr
	 */
	public class Main extends Sprite 
	{
		
		public function Main():void 
		{
			trace("Der Fixpunkt der Cosinus>Funktion liegt bei ",
				SecantSolver.solve(new CosinusFixpoint(), 0.0, 1.0, 0.0001).toFixed(3));
		}
		
	}
	
}
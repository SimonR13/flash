package  
{
	/**
	 * ...
	 * @author sr
	 */
	public class Coffee implements ICoffee 
	{
		private static var coffeePrice:Number = 0.99;
		private static var coffeeName:String = "coffee";
		
		public function Coffee() 
		{
			
		}
		
		public function get name(): String
		{
			return coffeeName;
		}
		
		public function get price(): Number
		{
			return coffeePrice;
		}
		
	}

}
package  
{
	/**
	 * ...
	 * @author sr
	 */
	public class Milk extends ExtrasDecorator 
	{
		private static var milkPrice:Number = 0.30;
		private static var milkName:String = "milk";
		
		public function Milk(_coffee:ICoffee) 
		{
			super(_coffee);
		}
		
		override public function get name(): String
		{
			var connector:String = " with ";
			
			if (_coffee is ExtrasDecorator) {
				connector = " and ";
			}

			return this._coffee.name + connector + milkName;
		}
		
		override public function get price(): Number
		{
			return this._coffee.price + milkPrice;
		}
		
	}

}
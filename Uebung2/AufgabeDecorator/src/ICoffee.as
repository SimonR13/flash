package  
{
	
	/**
	 * ...
	 * @author sr
	 */
	public interface ICoffee 
	{
		function get name(): String;
		function get price(): Number;
	}
	
}
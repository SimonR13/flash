package  
{
	/**
	 * ...
	 * @author sr
	 */
	public class Espresso implements ICoffee 
	{
		private static var espressoPrice:Number = 0.79;
		private static var espressoName:String = "espresso";
		
		public function Espresso() 
		{
			
		}
		
		public function get name(): String
		{
			return espressoName;
		}
		
		public function get price(): Number
		{
			return espressoPrice;
		}
	}

}
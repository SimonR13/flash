package  
{
	/**
	 * ...
	 * @author sr
	 */
	public class ExtrasDecorator implements ICoffee
	{
		protected var _coffee:ICoffee;
		
		public function ExtrasDecorator(coffee:ICoffee) {
			this._coffee = coffee;
		}
		
		public function get name(): String {
			return this._coffee.name;
		}
		public function get price(): Number {
			return this._coffee.price;
		}
		
	}

}
package  
{
	/**
	 * ...
	 * @author sr
	 */
	public class Sugar extends ExtrasDecorator 
	{
		private static var sugarPrice:Number = 0.10;
		private static var sugarName:String = "sugar";
		
		public function Sugar(_coffee:ICoffee) 
		{
			super(_coffee);
		}
		
		override public function get name(): String
		{
			var connector:String = " with ";
			
			if (_coffee is ExtrasDecorator) {
				connector = " and ";
			}
			
			return this._coffee.name + connector + sugarName;
		}
		
		override public function get price(): Number
		{
			return this._coffee.price + sugarPrice;
		}
	}

}
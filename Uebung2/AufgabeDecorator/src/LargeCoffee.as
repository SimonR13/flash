package  
{
	/**
	 * ...
	 * @author sr
	 */
	public class LargeCoffee implements ICoffee 
	{
		private static var lCoffeePrice:Number = 1.89;
		private static var lCoffeeName:String = "large coffee";
		
		public function LargeCoffee() 
		{
			
		}
		
		public function get name(): String
		{
			return lCoffeeName;
		}
		
		public function get price(): Number
		{
			return lCoffeePrice;
		}
		
	}

}
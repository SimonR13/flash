package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author sr
	 */
	public class Main extends Sprite 
	{
		
		public function Main():void 
		{
			var	myCoffee:ICoffee =	new	Coffee();
			trace(myCoffee.name +	": " + myCoffee.price.toFixed(2) + " EUR");
			trace("--------");
			myCoffee = new Sugar(new Coffee());
			trace(myCoffee.name +	": " + myCoffee.price.toFixed(2) + " EUR");
			trace("--------");
			myCoffee = new Milk(new Coffee());
			trace(myCoffee.name +	": " + myCoffee.price.toFixed(2) + " EUR");
			trace("--------");
			myCoffee = new Milk(new Sugar(new Coffee()));
			trace(myCoffee.name +	": " + myCoffee.price.toFixed(2) + " EUR");
			trace("--------");
			myCoffee = new Milk(new Sugar(new LargeCoffee()));
			trace(myCoffee.name +	": " + myCoffee.price.toFixed(2) + " EUR");
			trace("--------");
			myCoffee = new Sugar(new Espresso());
			trace(myCoffee.name +	": " + myCoffee.price.toFixed(2) + " EUR");
			trace("--------");
		}
	}
	
}
package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author sr
	 */
	public class Main extends Sprite 
	{
		
		private const NameList:Array = new Array( "Justus, Peter und Bob", "Christian", "Susi und Strolch", "Heinz", "Daniel, Gert" );
		private var NewNameList:Array = new Array();
		private var undRegExp:RegExp = /.und./g;
		private var kommaRegExp:RegExp = /[a-zA-Z]+[^, ]/g;
		
		public function Main():void 
		{
			var tmpArray:Array = NameList.map(splitNames);
			
			for each (var namenArr:Array in tmpArray) {
				for each (var name:String in namenArr) {
					NewNameList.push(name);	
				}
			}
			
			NewNameList.sort(Array.DESCENDING);
			trace(NewNameList);
		}
		
		private function splitNames(element:*, index:int, tmpNameList:Array):Array {
			var tmpStr:String = element.replace(undRegExp, ", ");
			var tmp:Array = tmpStr.match(kommaRegExp);
			
			return tmp;
		}
	}
	
}
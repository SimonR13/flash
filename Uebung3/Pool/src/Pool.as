package  
{
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author sr
	 */
	public class Pool
	{
		
		private var pool:Array;
		private var counter:int;
		private var type:Class;
				
		public function Pool(len:int, func:Function)
		{
			this.pool = new Array();
			this.counter = len;
			//this.type = type;
			
			for (var i:Number = 0; i < counter; i++) {
				this.pool[i] = func.call();
			}
		}
		
		public function requestItem(): Object
		{
			var item:* = null;
			if (counter > 0) 
			{
				counter--;
				item = this.pool.pop();
				item.enterSimulation();
			}
			
			return item;
		}
		
		public function returnItem( obj:Object ): void
		{
			counter++;
			obj.leaveSimulation()
			pool.push(obj);
		}
		
	}

}
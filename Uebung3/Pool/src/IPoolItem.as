package  
{
	
	/**
	 * ...
	 * @author sr
	 */
	public interface IPoolItem 
	{
		
		function enterSimulation(): void;
		
		function leaveSimulation(): void;
		
		function get inUse(): Boolean;
		
	}
	
}
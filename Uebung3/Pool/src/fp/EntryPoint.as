package fp
{
	import flash.display.Sprite;
	
	import fp.lib.text.Console;
	
	public class EntryPoint extends Sprite
	{
		private var _console:Console;
		
		public function get console():Console
		{
			return _console;
		}
		
		public function EntryPoint()
		{
			_console = new Console(this, stage.stageWidth, stage.stageHeight, 0x000000);
		}
	}
}
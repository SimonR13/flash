package fp.lib.display
{
	import flash.display.Sprite;

	public final class BasicShapes
	{
		public static function getRectangle(width:Number, height:Number, color:uint = 0, alpha:Number = 1, border:Number = 1): Sprite
		{
			var rectangle:Sprite = new Sprite();
			rectangle.graphics.lineStyle(border);
			rectangle.graphics.beginFill(color, alpha);
			rectangle.graphics.drawRect(0, 0, width, height);
			rectangle.graphics.endFill();
			return rectangle;
		}
		
		public function BasicShapes()
		{
			throw new ArgumentError("BasicShapes is a static utility class.");
		}
	}
}
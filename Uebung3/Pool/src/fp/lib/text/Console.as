package fp.lib.text
{
	import flash.display.*;
	import flash.text.*;
	
	public class Console extends TextField
	{	
		public function Console(parent:DisplayObjectContainer, width:uint, height:uint, color:uint = 0x000000)
		{			
			this.width = width;
			this.height = height;
			
			background = true;
			backgroundColor = color;
						
			defaultTextFormat = new TextFormat("Consolas", 12, 0xffffff - color);
			
			multiline = wordWrap = true;
			
			parent.addChild(this);
		}
		
		public function print(...args): void
		{
			for (var i:int=0; i<args.length; i++)
			{
				if (args[i] == null)
				{
					appendText("null\t");
				}
				else
				{
					appendText(args[i].toString() + "\t");	
				}
			}
			appendText("\n");
			scrollV++;	
		}
		
		public function clear(): void
		{
			text = "";
			scrollV = 0;
		}
	}
}
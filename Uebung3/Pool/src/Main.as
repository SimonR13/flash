package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import fp.EntryPoint;
	
	/**
	 * ...
	 * @author sr
	 */
	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class Main extends EntryPoint 
	{
		public function Main():void 
		{
			var	pool:Pool =	new	Pool(3,	SimplePoolItem.create);	
			
			var p1:SimplePoolItem = pool.requestItem() as SimplePoolItem;
			var p2:SimplePoolItem = pool.requestItem() as SimplePoolItem;
			var p3:SimplePoolItem = pool.requestItem() as SimplePoolItem;
			
			console.print("Requested three items:");
			console.print(p1, p1.inUse);
			console.print(p2, p2.inUse);
			console.print(p3, p3.inUse);
			
			/*var	p4:SimplePoolItem =	pool.requestItem() as SimplePoolItem;
			console.print("Requested fourth item:");
			console.print(p4, p4.inUse);*/
			
			console.print("Returned an item:");
			pool.returnItem(p2);
			console.print(p2, p2.inUse);
			
			var p5:SimplePoolItem = pool.requestItem() as SimplePoolItem;
			console.print("Requested an item:");
			console.print(p5, p5.inUse);
			
			console.print("Finished successfully.");
		}
		
	}
	
}
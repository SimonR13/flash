package  
{
	/**
	 * ...
	 * @author sr
	 */
	public class SimplePoolItem implements IPoolItem
	{
		
		private var simulationStatus:Boolean;
		
		public function SimplePoolItem() 
		{
			this.simulationStatus = false;
		}
		
		public static function create(): SimplePoolItem
		{
			return new SimplePoolItem();
		}
		
		public function get inUse(): Boolean
		{
			return this.simulationStatus;
		}
		
		public function enterSimulation(): void
		{
			this.simulationStatus = true;
		}
		
		public function leaveSimulation(): void
		{
			this.simulationStatus = false;
		}
	}

}
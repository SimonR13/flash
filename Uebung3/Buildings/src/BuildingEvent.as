package  
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author sr
	 */
	public class BuildingEvent extends Event 
	{
		public static const PROGRESS:String = "PROGRESS";
		
		public function BuildingEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
			
		} 
		
		public override function clone():Event 
		{ 
			return new BuildingEvent(type, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("BuildingEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}
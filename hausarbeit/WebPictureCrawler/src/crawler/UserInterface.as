package crawler
{
	import events.RemovePictureEvent;
	import events.ShowBigPictureEvent;
	import events.ValidUrlEnteredEvent;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.graphics.codec.JPEGEncoder;
	
	import picstrip.PicStrip;
	
	import spark.components.Label;
	import spark.components.TitleWindow;
	
	
	public class UserInterface extends TitleWindow
	{
		private var gui:WebPictureCrawler;
		private var crwlr:Crawler;
		private var popUp:EnterUrlPopUp;
		private var previewStrip:PreviewPicStrip;
		private var eventDispatcher:EventDispatcher;
		private var origPicArray:Object = new Object();
		private var progress:int = 0;
		
		public function UserInterface( _gui:WebPictureCrawler )
		{
			super();
			gui = _gui;
			gui.imgContainer.drawRoundRect(0,0,605,605,null,0x000000,0.8);
			
			eventDispatcher = new EventDispatcher();
			eventDispatcher.addEventListener(ValidUrlEnteredEvent.ON_URL_ENTERED, loadCrawler);
			eventDispatcher.addEventListener(ShowBigPictureEvent.ON_SHOW_BIG, showBigPicture);
			eventDispatcher.addEventListener(RemovePictureEvent.ON_REMOVE_PICTURE, removePicture);
			
			initPopUp();
			
			initPreviewStrip();
			
			gui.btnSelectUrl.addEventListener(MouseEvent.CLICK, btnSelectUrl);
			gui.btnSaveAll.addEventListener(MouseEvent.CLICK, btnSaveAll);
			gui.btnSaveSelection.addEventListener(MouseEvent.CLICK, btnSaveSelection);
			
		}
		
		// instanziiert den UrlPopUp, zentriert und blendet ihn aus;
		private function initPopUp() : void
		{
			popUp = new EnterUrlPopUp();
			gui.addElementAt(popUp, 3);
			popUp.centerMe();			
			popUp.visible = false;
		}
		
		// instanziiert den Vorschaustreifen, positioniert und blendet ihn aus;
		private function initPreviewStrip() : void
		{
			previewStrip = new PreviewPicStrip();
			previewStrip.x = gui.viewstack1.width;
			previewStrip.y = -12;
			previewStrip.width = 570;
			previewStrip.height = 140;
			gui.addElementAt(previewStrip, 0);
			previewStrip.visible = false;
			previewStrip.eventDispatcher = eventDispatcher;
		}
		
		// behandelt das schliess-event des urlpopups
		protected function onClose(event:CloseEvent) : void
		{
			popUp.addEventListener(ValidUrlEnteredEvent.ON_URL_ENTERED, loadCrawler);
			popUp.removeEventListener(CloseEvent.CLOSE,onClose);
			popUp.visible = false;
		}
		
		// behandelt das aendern der groesse des fensters/stage
		// passt die elemente der hauptklasse direkt an und uebergibt dem Vorschaustreifen die neue breite
		public function handleResize() : void
		{
			gui.enabled = false;
			var _newWidth:uint = gui.stage.stageWidth as uint;
			var _newHeight:uint = gui.stage.stageHeight as uint;
			previewStrip.resizeWidth = _newWidth - gui.viewstack1.width as uint;
			
			gui.imgContainer.bottom = ((_newHeight - gui.viewstack1.height) / 2) - 300;

			gui.bigPictureLabel.horizontalCenter = 0;
			gui.bigPictureLabel.y = gui.imgContainer.x + gui.imgContainer.height + 20;
			
			gui.resizeExecuting = false;
			gui.enabled = true;
		}
		
		// zum auswahl der URL, oeffnet den PopUp
		protected function btnSelectUrl(event:MouseEvent):void
		{
			// open PopUp for URL
			popUp.visible = true;
			popUp.addEventListener(CloseEvent.CLOSE, onClose);
			popUp.addEventListener(ValidUrlEnteredEvent.ON_URL_ENTERED, loadCrawler);
			popUp.setFocus();
			popUp.txtInputUrlPopUp.setFocus();
		}
		
		// zum speichern der ausgewaehlten bilder; 
		// bei einem bild kann der name direkt im speichern dialog angegeben werden
		// bei mehreren wird der ordner-dialog angezeigt und die namen der bilder aus dem html benutzt
		protected function btnSaveSelection(event:MouseEvent):void
		{
			// open FolderSelector and save selected picture(s)
			var file:File = new File();
			var selPicArr:ArrayList = previewStrip.selectedPics;
			if (selPicArr.length == 1) {
				file.addEventListener(Event.SELECT, saveSelectedToFile);
				file.browseForSave("Bild speichern...");
			}
			else if (selPicArr.length > 1) {
				file.addEventListener(Event.SELECT, saveSelectedToFolder);
				file.browseForDirectory("Ordner zum Speichern w&auml;hlen...");
			}
			trace("btnSaveSelect", event.toString());
		}
		
		// speichern aller bilder; oeffnet ordner-dialog und speichert die bilder unter dem html-namen
		protected function btnSaveAll(event:MouseEvent):void
		{
			trace("btnSaveAll", event.toString());
			// todo 
			var file:File = new File;
			file.addEventListener(Event.SELECT, saveAllToFolder);
			file.browseForDirectory("Ordner zum Speichern w&auml;hlen...");
			
		}

		// einzelnes bild speichern, aufruf nach auswahl im dialog
		private function saveSelectedToFile( event:Event ) : void
		{
			event.target.removeEventListener(Event.SELECT, saveSelectedToFile);
			var file:File = event.currentTarget as File;
			
			var fileName:String = file.nativePath.substring(file.nativePath.lastIndexOf("\\") + 1) as String;
			trace("saveSelectedToFile", file.nativePath, "fileName", fileName);
			fileName = fileName.search(".") != -1 ? fileName + ".jpeg" : fileName.substring(0, fileName.lastIndexOf(".")) + ".jpeg";
			var tmpFile:File = file.resolvePath(file.nativePath.substring(0, file.nativePath.lastIndexOf("\\"))) as File;
			var tmpArray:Object = new Object();
			var _key:String = previewStrip.selectedPics.getItemAt(0) as String;
			tmpArray[_key] = origPicArray[_key] as Bitmap;
			
			saveToFolder(tmpFile, tmpArray, fileName);			
		}
		
		// mehrere bilder speichern, aufruf nach auswahl im dialog
		private function saveSelectedToFolder(event:Event) : void 
		{
			event.target.removeEventListener(Event.SELECT, saveSelectedToFolder);
			var file:File = event.currentTarget as File;

			var selPicArrayList:ArrayList = previewStrip.selectedPics as ArrayList;
			var tmpArray:Object = new Object();
			for ( var i:int = 0; i < selPicArrayList.length; ++i) {
				var _key:String = selPicArrayList.getItemAt(i) as String
				tmpArray[_key] = origPicArray[_key];
			}
			
			saveToFolder(file, tmpArray);
			
		}
		
		// alle bilder speichern, aufruf nach auswahl im dialog
		private function saveAllToFolder( event:Event ) : void
		{
			event.target.removeEventListener(Event.SELECT, saveAllToFolder);
			var file:File = event.currentTarget as File;
			trace("saveAllToFolder", file.nativePath);
			
			saveToFolder(file, origPicArray);
		}
		
		// codieren der bilder als jpeg und physikalisches speichern;
		private function saveToFolder( _file:File, _picToSaveArray:Object, _fileName:String = null) : void
		{
			var fileAccess:FileStream = new FileStream();
			var jpgeEnc:JPEGEncoder = new JPEGEncoder();
			var tmpFile:File = new File();
			
			for ( var _key:String in _picToSaveArray ) {
				var fileName:String = _key.substring(0, _key.lastIndexOf("_"));
				fileName = _fileName == null ? fileName.substring(0, fileName.lastIndexOf(".")) + ".jpeg" : _fileName;
				tmpFile = tmpFile.resolvePath(_file.nativePath + "\\" + fileName);
				var bmp:Bitmap = _picToSaveArray[_key] as Bitmap;
				var ba:ByteArray = jpgeEnc.encode(bmp.bitmapData as BitmapData) as ByteArray;
				
				fileAccess.open(tmpFile, FileMode.WRITE);
				fileAccess.writeBytes(ba, 0, ba.length);
				fileAccess.close();
				tmpFile = new File();
				fileAccess = new FileStream();
			}
		}
		
		// anstossen der crawl funktion nach eingabe einer gueltigen url 
		public function loadCrawler(event:ValidUrlEnteredEvent) : void
		{
			popUp.removeEventListener(ValidUrlEnteredEvent.ON_URL_ENTERED, loadCrawler);
			popUp.visible = false;
			gui.enabled = false;

			if (crwlr == null) {
				crwlr = new Crawler(eventDispatcher, event._url);
			}
			else {
				crwlr.setUrl(event._url);
			}
			
			previewStrip.resetStrip();
			origPicArray = new Object();
			eventDispatcher.addEventListener(Event.COMPLETE, crawlerDone);
			eventDispatcher.addEventListener(IOErrorEvent.IO_ERROR, handleIOError);
		}
		
		// crawler ist fertig und haelt urls zu den bildern in array vor
		// array wird durchlaufen und einem loader uebergeben der die resourcen aus dem internet laedt
		protected function crawlerDone(event:Event) : void
		{
			event.target.removeEventListener("COMPLETE", crawlerDone);
			eventDispatcher.removeEventListener(IOErrorEvent.IO_ERROR, handleIOError);
			
			for (var i:int = 0; i < crwlr.getArrayLength(); ++i) { 
				var imageLoader:Loader = new Loader();
				imageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, populatePreview);
				var picArray:Array = crwlr.getArray();
				var url:String = picArray[i];
				if (url == "") continue;
				imageLoader.name = url;
				imageLoader.load(new URLRequest(url));
				progress++;
				imageLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, handleIOError);
			}

		}
		
		// hat ein loader fertig geladen wird das geladene bild an die vorschau uebergeben
		private function populatePreview( event:Event ) : void
		{
			var loader:Loader = event.target.loader as Loader;
			if (loader.width > 1 || loader.height > 1)
			{
				var url:String = loader.contentLoaderInfo.url as String;
				var _key:String = url.substr(url.lastIndexOf("/") + 1,url.length-1) + "_" + (new Date).time;
				var img:Bitmap = event.target.content as Bitmap;
				origPicArray[_key] = img;
				
				var clonedBmpData:BitmapData = img.bitmapData.clone();
				var cloneImage:Bitmap = new Bitmap(clonedBmpData);
				previewStrip.addPicture(_key, cloneImage);
			}
			loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, populatePreview);
			
			if (--progress <= 0) {			
				gui.enabled = true;
			}
			previewStrip.alpha = 1;
			previewStrip.visible = true;
		}

		// aufruf durch ein event aus der vorschau;
		// entfernt das original bild aus dem origPicArray
		private function removePicture( event:RemovePictureEvent ) : void 
		{
			var _key:String = event.key as String;
			
			delete origPicArray[_key];
			
		}
		
		// event aus der vorschau
		// fuegt eine kopie des original bildes auf die buehne hinzu 
		private function showBigPicture( event:ShowBigPictureEvent ) : void
		{
			gui.enabled = false;
			gui.imgContainer.removeAllElements();
			
			var key:String = event.key as String;
			var img:Bitmap = origPicArray[key] as Bitmap;
			var uiComp:UIComponent = new UIComponent;
			var lblKey:Label = gui.bigPictureLabel;
			lblKey.text = key.substring(0, key.lastIndexOf("_"));
			uiComp.addChild(img);
			if (img.width > 600) {
				img.scaleX = img.scaleY = 600 / img.width;
			}
			
			img.x = gui.imgContainer.width / 2 - img.width / 2;
			img.y = gui.imgContainer.height / 2 - img.height / 2;
			
			gui.imgContainer.addElement(uiComp);
			gui.enabled = true;
		}
		
		public function handleIOError(event:IOErrorEvent):void 
		{
			eventDispatcher.removeEventListener(IOErrorEvent.IO_ERROR, handleIOError);
			progress--;
			
			if(event.errorID == 2124)
			{	
				Alert.show("Unbekanntes Dateiformat in der Sequenz vorhanden" );
				gui.enabled = true;
				return;
			}
			if (event.errorID == PicStrip.NO_ELEMENT_ERROR_ID) {
				Alert.show(event.text.toString(), "Fehler bei der Ausführung...");
			}
			Alert.show("Server nicht erreichbar...");
			gui.enabled = true;
			
		}
	} 
}
package utils
{
	import mx.validators.ValidationResult;
	import mx.validators.Validator;
	
	public class UrlValidator extends Validator
	{
		public function UrlValidator()
		{
			super();
		}
		
		protected override function doValidation(value:Object):Array
		{
			var results:Array = [];
			
			results = super.doValidation(value);
			if (results.length > 0) 
			{
				return results;
			}
			var regEx:RegExp = /^((http:\/\/www\.)|(www\.)|(http:\/\/))[a-zA-Z0-9._-]+\.[a-zA-Z.]{2,5}(\/)?([\d\w])*$/
			if (!regEx.test(value.toString()))
			{
				results.push(new ValidationResult(true, null, "URL ungueltig", "Bitte URL ueberpruefen."));
				return results;
			}
			return results;
		}
	}
}
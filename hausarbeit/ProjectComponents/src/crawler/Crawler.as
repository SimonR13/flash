package crawler
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	
	import mx.controls.Alert;
	
	public class Crawler
	{
		
		private var eventDispatcher:EventDispatcher;
		
		private var url:String;
		private var req:URLRequest;
		private var loader:URLLoader;
		private var HTMLStr:String;
		
		private var picArray:Array;
		private var splittedArray:Array
		
		public function Crawler(_dispatcher:EventDispatcher, _url:String)
		{
			this.eventDispatcher = _dispatcher;
			this.url = _url;
			
			crawl();
		}
		
		private function crawl() : void
		{
			
			if(url.search("http://") == -1)
			{
				url = "http://" + url;
			}
			this.req = new URLRequest(url);
			this.loader = new URLLoader(req);

		 
			this.loader.addEventListener(IOErrorEvent.IO_ERROR, handleIOErrors);
	
			function handleIOErrors(event:IOErrorEvent):void 
			{
				eventDispatcher.dispatchEvent(new IOErrorEvent(IOErrorEvent.IO_ERROR));
			}
			
			this.req.method = URLRequestMethod.GET;
			
			loader.addEventListener(Event.COMPLETE, onPageLoaded);
			loader.load(req);
		}
		
		private function onPageLoaded(e:Event):void
		{
			var tmpArray:Array;
			var tmp2Array:Array;
			var tmp3Array:Array;
			var tmp4Array:Array;
			var tmp5Array:Array;
			HTMLStr = loader.data.toString();
			var regExp:RegExp = /<img[^>]+>/gi;
			var regExp2:RegExp = /(src)(=)(".*?")/gi;
			var regExp3:RegExp = /http:\/\/(".*")("\/")/gi;
			var StrStr:String = "";
			
			picArray = regExp.exec(HTMLStr);
			tmpArray = picArray;
			
			if(picArray == null)
			{
				Alert.show("Es konnten keine Bilder aus der Website geparsed werden!");
				trace("Es konnten keine Bilder aus der Website geparsed werden!");
			}
			
			var i:int = 1;
			while(picArray != null)
			{		
				
				
				picArray = regExp.exec(HTMLStr);
				tmpArray[i] = picArray;
				i++;
			}
			
			
			
			var tmpArrayString:String = tmpArray.join();
			
			tmp2Array = regExp2.exec(tmpArrayString);
			
			tmp3Array = tmp2Array;
				
			var j:int = 1
			while(tmp2Array != null)
			{		
				StrStr += tmp2Array[3] +" \n";
				tmp2Array = regExp2.exec(tmpArrayString);
				tmp3Array[j] = tmp2Array;
				
				j++;
			}
			
			splittedArray = StrStr.split("\n")
			
			for(var k:int = 0; k < splittedArray.length-1; k++)
			{
				
				splittedArray[k] = splittedArray[k].toString().substring(1,splittedArray[k].toString().length-2);
				
				
				if(splittedArray[k].search(regExp3.test(splittedArray[k])) == -1)
				{
					splittedArray[k] = "/" + splittedArray[k].toString();
					//trace(splittedArray[k]);
				}
				
				if(splittedArray[k].search("//") != -1)
				{
					splittedArray[k] = splittedArray[k].toString().substring(1,splittedArray[k].toString().length);
				}
				
				if(splittedArray[k].search("/http://") != -1)
				{
					//trace("test");
					splittedArray[k] = splittedArray[k].toString().substring(1,splittedArray[k].toString().length);
					//splittedArray[k] = _URL + splittedArray[k].toString();
				}
				
				
				if(splittedArray[k].search("http://") == -1)
				{
					//trace(splittedArray[k]);
					splittedArray[k] = url + splittedArray[k].toString();
					
				}		
				trace(splittedArray[k]);
				
			}
			
			eventDispatcher.dispatchEvent(new Event(Event.COMPLETE));
		}
		
		public function getArray():Array
		{
			return splittedArray;
		}
		
		public function getArrayLength():int
		{
			return splittedArray.length-1;		
		}
		
		public function setUrl( _url:String) : void
		{
			this.url = _url;
			crawl();
		}
}
}

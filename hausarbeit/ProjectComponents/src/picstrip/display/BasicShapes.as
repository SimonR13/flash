package picstrip.display
	
	// BasicShapes
	// Stammt aus der Vorlesung Flash der Hochschule Trier
	// wurde um die Funktion getCross erweitert, die ein Kreuz erstellt auf einem beliebigen Hintergrund; 
	
{
	import flash.display.Sprite;

	public final class BasicShapes
	{
		public static function getRectangle(width:Number, height:Number, color:uint = 0, alpha:Number = 1, border:Number = 1): Sprite
		{
			var rectangle:Sprite = new Sprite();
			rectangle.graphics.lineStyle(border);
			rectangle.graphics.beginFill(color, alpha);
			rectangle.graphics.drawRect(0, 0, width, height);
			rectangle.graphics.endFill();
			return rectangle;
		}
		
		public static function getCross(width:Number, height:Number, color:uint = 0, alpha:Number = 1, bgColor:uint = 0, bgAlpha:Number = 0, border:Number = 1): Sprite
		{
			var cross:Sprite = new Sprite();
			cross.graphics.lineStyle(border, color, alpha);
			cross.graphics.moveTo(0,0);
			cross.graphics.lineTo(width, height);
			cross.graphics.moveTo(width,0);
			cross.graphics.lineTo(0, height);
			var crossRect:Sprite = getRectangle(width, height, bgColor, bgAlpha, 0);
			crossRect.addChild(cross);
			return crossRect;
		}
		
		public function BasicShapes()
		{
			throw new ArgumentError("BasicShapes is a static utility class.");
		}
	}
}
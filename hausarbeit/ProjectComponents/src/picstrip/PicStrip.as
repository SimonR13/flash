package picstrip
{
	import events.RemovePictureEvent;
	import events.ShowBigPictureEvent;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayList;
	import mx.core.UIComponent;
	
	import picstrip.display.BasicShapes;
	
	import spark.components.Button;
	import spark.components.HGroup;
	
	public class PicStrip extends Sprite implements IEventDispatcher
	{
		public static const NO_ELEMENT_ERROR_ID:int = 404;
		public static const MOUSE_OVER_LAYER:String = "mouseOverLayer";
		public static const CROSS_OVER_LAYER:String = "crossOverLayer";
		public static const PICTURE_SELECTED_RECTANGLE:String = "picSelectedRect";
	
		private static var PicStripINSTANCE:PicStrip = new PicStrip();
		
		private var eventDispatcher:EventDispatcher;
		private var gui:PreviewPicStrip;
		private var picArray:Object = new Object();
		private var keyArray:ArrayList = new ArrayList();
		private var selectedPicArray:Object = new Object();
		private var maxPicsPerPage:int = 1;
		private var picsPerLastPage:int = 1;
		private var currPage:int = 0; 
		private var pages:int = 1; 
		private var hGrp:HGroup;
		
		private const STARTED_EVENT:Event = new Event("STARTED"); 
		private const COMPLETED_EVENT:Event = new Event("COMPLETED"); 
		private static const NO_ELEMENT_IN_ARRAY:String = "Kein Bild zum Anzeigen verfügbar!";
		
		public function PicStrip() 
		{
			if (PicStripINSTANCE) {
				throw new Error ("Keine weitere Instance erlaubt!");
			}
		}
		
		public static function getInstance() : PicStrip
		{
			return PicStripINSTANCE;
		}
		
		// eigenes EventDispatcher-Object zum einfachen weiterreichen von Events
		public function setEventDispatcher( _eventDisp:EventDispatcher ) : void
		{
			eventDispatcher = _eventDisp;
		}
		
		// global eindeutiger schluessel fuer identifizieren bei entfernen und vergroessern des Bildes;
		// fuegt Bild im Bitmap-Format der Vorschau hinzu; 
		public function addPicture( _key:String, _val:Bitmap ) : void
		{
			dispatchEvent(STARTED_EVENT);
			picArray[_key] = _val;
			keyArray.addItem(_key);
			selectedPicArray[_key] = null;
			getPageCount();
			toggleButtons();
			reDrawStrip();
		}
		
		// Stellt Verbindung zur Oberflaeche der Vorschau dar;
		// vereinfacht interaktion mit den elementen; 
		public function setGui( _gui:PreviewPicStrip ) : void
		{
			this.gui = _gui;
			this.hGrp = this.gui.hGroup as HGroup;
			this.gui.btnLeft.addEventListener(MouseEvent.CLICK, btnLeftClicked);
			this.gui.btnRight.addEventListener(MouseEvent.CLICK, btnRightClicked);
		}
		
		// behandelt die Resize-Funktion;
		public function set newWidth( _newWidth:uint ) : void
		{
			gui.width = _newWidth;
			hGrp.width = _newWidth - ( gui.btnLeft.width + gui.btnRight.width);
			hGrp.horizontalCenter = 0;
			hGrp.horizontalAlign= "center";
			getPageCount();
			toggleButtons();
			reDrawStrip();
		}
		
		// beim eingeben einer neuen adresse wird zuerst die vorschau resettet;
		// dabei werden die arrays neu initialisiert und die navigations-variablen zurueckgesetzt;
		public function resetStrip() : void
		{
			picArray = new Object();
			keyArray = new ArrayList();
			selectedPicArray = new Object();
			maxPicsPerPage = pages = 1;
			currPage = 0;
			getPageCount();
			toggleButtons();
			reDrawStrip();
		}
		
		// blaettert in der vorschau nach links
		private function btnLeftClicked( event:MouseEvent ) : void
		{
			currPage -= 1;
			toggleButtons();
			reDrawStrip();
		}
		
		// blaettert in der vorschau nach rechts
		private function btnRightClicked( event:MouseEvent) : void
		{
			currPage += 1;
			toggleButtons();
			reDrawStrip();
		}
		
		// funktion zum de/aktivieren der blaetter-buttons
		// rechter button enabled: mehr als 1 seite, aber nicht auf der letzten
		// linker button enabled: nicht auf seite 1;
		private function toggleButtons() : void
		{
			var btnLeft:Button = gui.btnLeft;
			var btnRight:Button = gui.btnRight;
			var stateLeft:Boolean = false;
			var stateRight:Boolean = false;
			
			if (pages < 2) {
				stateLeft = stateRight = false;
			}
			else {
				if (currPage < 1) {
					stateLeft = false;
					stateRight = true;
				}
				else if (currPage > 0 && currPage < pages-1) {
					stateLeft = stateRight = true;
				}
				else {
					stateLeft = true;
					stateRight = false;
				}
			}
			
			btnLeft.enabled = btnLeft.mouseEnabled = stateLeft;
			btnRight.enabled = btnRight.mouseEnabled = stateRight;
		}
		
		// fuegt die bilder aus dem picArray abhaengig des platzes in die vorschau
		// skaliert und mit eventlistener fuer click und hover
		public function reDrawStrip() : void
		{
			hGrp.removeAllElements();
			
			var arrayCeiling:int = currPage != pages-1 ? maxPicsPerPage : picsPerLastPage;
			for ( var i:int = 0; i < arrayCeiling; ++i ) 
			{
				var _key:String = keyArray.getItemAt(i + (currPage*maxPicsPerPage)) as String;
				if (picArray[_key] == null) { continue; }
				var img:Bitmap = picArray[_key] as Bitmap;
				var uiComp:UIComponent = new UIComponent();
				uiComp.name = _key;
				
				uiComp.height = 95;
				uiComp.width = 95;
				uiComp.x = i * 95;
				uiComp.y = 0;
				uiComp.addChild(BasicShapes.getRectangle(uiComp.width, uiComp.height, 0x808080));
				uiComp.drawFocus(true);
				uiComp.includeInLayout = true;
				hGrp.addElement(uiComp);
				
				uiComp.addChild(img);
				img.scaleX = 90 / img.width > 1 ? 1 : 90 / img.width;
				img.scaleY = 90 / img.height > 1 ? 1 : 90 / img.height;
				if (img.width > 90)
					img.width = 90;
				if (img.height > 90)
					img.height = 90;

				if (selectedPicArray[_key] != null) {
					addPicSelectedLayer(uiComp, _key);
				}
					
				uiComp.addEventListener(MouseEvent.ROLL_OVER , mouseOverUiComp);
				uiComp.addEventListener(MouseEvent.CLICK, picSelect);
			}
			gui.addElement(gui.btnLeft);
			gui.addElement(gui.btnRight);
			eventDispatcher.dispatchEvent(COMPLETED_EVENT);
		}
		
		// erster teil des "maus-ueber-bild"-event: zeigt rahmen mit kreuz an; 
		private function mouseOverUiComp( event:MouseEvent ) : void 
		{
			event.target.removeEventListener(MouseEvent.ROLL_OVER, mouseOverUiComp);
			var uiComp:UIComponent = event.currentTarget as UIComponent;
			var layer:Sprite = getLayer(uiComp.width, uiComp.height);
			var selRect:Sprite = selectedPicArray[uiComp.name] as Sprite;
			
			layer.name = PicStrip.MOUSE_OVER_LAYER;
			uiComp.addChild(layer);
			if (selRect != null) {
				uiComp.swapChildren(selRect, layer);
			}
			uiComp.addEventListener(MouseEvent.ROLL_OUT, mouseOutUiComp);
		}
		
		// zweiter teil des "maus-ueber-bild"-event: entfernt rahmen;
		private function mouseOutUiComp( event:MouseEvent ) : void
		{
			event.target.removeEventListener(MouseEvent.ROLL_OUT, mouseOutUiComp);
			var uiComp:UIComponent = event.currentTarget as UIComponent;
			
			uiComp.removeChild(uiComp.getChildByName(PicStrip.MOUSE_OVER_LAYER));
			uiComp.addEventListener(MouseEvent.ROLL_OVER, mouseOverUiComp);
		}
		
		// zum selektieren eines/mehrerer Bilder mit STRG/CTRL
		// Bild ohne STRG/CTRL angelickt: nur geklicktes wird ausgewaehlt, andere werden abgewaehlt;
		// Bild mit STRG/CTRL: nicht gewaehltes bild wird angewaehlt;
		private function picSelect( event:MouseEvent ) : void
		{
			var uiComp:UIComponent = event.currentTarget as UIComponent;
			if (uiComp) {
				var _key:String = uiComp.name;
				if (!event.ctrlKey) {
					for ( var _layerKey:String in selectedPicArray ) {
						var layer:Sprite = selectedPicArray[_layerKey] as Sprite;
						if (layer == null) { 
							continue; 
						}
						var parent:UIComponent = layer.parent as UIComponent;
						parent.removeChild(layer);
						selectedPicArray[_layerKey] = null;
					}
				}
				var bPEvent:ShowBigPictureEvent = new ShowBigPictureEvent(ShowBigPictureEvent.ON_SHOW_BIG, _key);
				eventDispatcher.dispatchEvent(bPEvent);
				addPicSelectedLayer(uiComp, _key);
			}
			
		}

		// erstellt das sprite fuer den ausgewaehlt-rahmen und fuegt ihn an die uiComp;
		// ist rahmen schon vorhanden wird vorhandener rahmen angefuegt => kommt in den vordergrund
		// notwendig zB bei umblaettern zw auswaehlen
		private function addPicSelectedLayer(uiComp:UIComponent, _key:String) : void
		{
			var selRect:Sprite;
			if (selectedPicArray[_key] != null) {
				selRect = selectedPicArray[_key] as Sprite;
			}
			else {
				selRect = BasicShapes.getRectangle(uiComp.width, uiComp.height, 0x00ff00, 0.1, 2);
				selRect.name=PicStrip.PICTURE_SELECTED_RECTANGLE;
				selRect.addEventListener(MouseEvent.CLICK, picUnSelect, false, 5);
				selectedPicArray[_key] = selRect;
			}
			uiComp.addChild(selRect);
		}
		
		// ohne STRG/CTRL wird nur das angeklickte ausgewaehlt
		// mit STRG/CTRL wird es abgewaehlt
		private function picUnSelect( event:MouseEvent ) : void
		{
			event.target.removeEventListener(MouseEvent.CLICK, picUnSelect, false);
			
			var selRect:Sprite = event.currentTarget as Sprite;
			if (selRect) {
				var uiComp:UIComponent = event.currentTarget.parent as UIComponent;
				var _key:String = uiComp.name;
				if (!event.ctrlKey) {
					for ( var layerKey:String in selectedPicArray ) {
						if ( _key == layerKey ) { continue; }
						var layer:Sprite = selectedPicArray[layerKey] as Sprite;
						var parent:UIComponent = layer.parent as UIComponent;
						parent.removeChild(layer);
						parent = null;
						layer = null;
						selectedPicArray[layerKey] = null;
					}
					selectedPicArray[_key] = selRect;
					uiComp.addChild(selRect);
				}
				else if (event.ctrlKey) {
					uiComp.removeChild(selRect);
					selectedPicArray[_key] = null;
					event.stopImmediatePropagation();
				}
			} 
		}
		
		// faengt event von rahmen ab und gibt entfernt das bild aus picArray und selectedPicArray,
		// zeichnet die Vorschau neu mit aktualiserten seiten/bilderanzahl
		// gibt key als event an hauptklasse zum entfernen des bildes aus dem Hauptarray
		// haelt die weitere verarbeitung des Events an, da das bild entfernt wurde
		private function picRemove( event:MouseEvent ) : void
		{
			event.stopImmediatePropagation();
			
			var cross:Sprite = event.currentTarget as Sprite;
			if (cross && cross.hitTestPoint(event.stageX, event.stageY)) {
				if (cross.parent is Sprite && cross.parent.parent is UIComponent) {
					var uiComp:UIComponent = cross.parent.parent as UIComponent;
					var _key:String = uiComp.name;
					
					cross.removeEventListener(MouseEvent.CLICK, picRemove);

					keyArray.removeItem(_key);
					
					delete picArray[_key];
					
					delete selectedPicArray[_key];
					
					var remPicEvent:RemovePictureEvent = new RemovePictureEvent(RemovePictureEvent.ON_REMOVE_PICTURE, _key);
					eventDispatcher.dispatchEvent(remPicEvent);
					
					var tmp:int = currPage;
					getPageCount(tmp);
					toggleButtons();
					reDrawStrip();
					
				}
			}
		}
		
		// generiert einen einfachen Rahmen der anzeigt ob die maus sich ueber dem bild befindet;
		private function getLayer(w:Number, h:Number) : Sprite
		{
			var focusLayer:Sprite = BasicShapes.getRectangle(w, h, 0x808080, 0.4, 2);
			var cross:Sprite = BasicShapes.getCross(10, 10, 0x000000, 1, 0x808080, 1, 3);
			cross.x = (focusLayer.width - cross.width) - 3;
			cross.y = 3;
			cross.name = PicStrip.CROSS_OVER_LAYER;
			cross.addEventListener(MouseEvent.CLICK, picRemove, false, 10);
			focusLayer.addChild(cross);
			return focusLayer;
		}
		
		// berechnet die anzahl der seiten sowie die maximale anzahl der bilder pro seite,
		// maximale anzahl bilder auf der letzten seite
		// beim loeschen eines bildes wird noch ein parameter der aktuellen seite mitgegeben
		// um auf der gerade angezeigten seite zu bleiben
		private function getPageCount( _currPage:int = 0) : void
		{
			var _stripWidth:int = hGrp.width;
			var _tmp:int = 0;
			
			if ((_stripWidth / 95) >= keyArray.length) 
			{
				maxPicsPerPage = picsPerLastPage = keyArray.length;
				pages = 1;
				currPage = _currPage;
			}
			else
			{
				maxPicsPerPage = Math.floor(_stripWidth / 95);
				pages = Math.ceil(keyArray.length / maxPicsPerPage);
				picsPerLastPage = (keyArray.length - ((pages-1) * maxPicsPerPage));
				currPage = _currPage;
			}
		}

		// getter um von der hauptklasse die selektierten bilder abzugreifen fuer die "Auswahl speicher"-Funktion
		public function get selPicArray() : ArrayList
		{
			var retArr:ArrayList = new ArrayList();
			for ( var _key:String in selectedPicArray ) 
			{
				if (selectedPicArray[_key] != null) {
					retArr.addItem(_key);
				}
			}
			return retArr;
		}
	}
}
package events
{
	import flash.events.Event;
	
	import mx.events.FlexEvent;
	
	public class RemovePictureEvent extends FlexEvent
	{
		public static const DEFAULT_NAME:String = "de.hausarbeit.events.RemovePictureEvent";
		public static const ON_REMOVE_PICTURE:String = "onRemovePicture";
		public var key:String;

		public function RemovePictureEvent(_type:String, _key:String, _bubbles:Boolean=false, _cancelable:Boolean=false)
		{
			super(_type, _bubbles, _cancelable);
			key = _key;
		}
		
		public override function clone() : Event
		{
			return new RemovePictureEvent(type, this.key, bubbles, cancelable);
		}
		
		public override function toString(): String
		{
			return formatToString("RemovePictureEvent", "key", "type", "bubbles", "cancelable");
		}
	}
}
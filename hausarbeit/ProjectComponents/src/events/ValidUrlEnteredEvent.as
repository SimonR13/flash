package events
{
	import flash.events.Event;
	import mx.events.FlexEvent;
	
	public class ValidUrlEnteredEvent extends FlexEvent
	{
		public static const DEFAULT_NAME:String = "de.hausarbeit.events.EnterUrlEvent";
		public static const ON_URL_ENTERED:String = "onValidUrlEntered";
		public var _url:String;
		
		public function ValidUrlEnteredEvent(type:String, url:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_url = url;
		}
		
		public override function clone() : Event
		{
			return new ValidUrlEnteredEvent(type, this._url, bubbles, cancelable);
		}
		
		public override function toString(): String
		{
			return formatToString("ValidUrlEnteredEvent", "_url", "type", "bubbles", "cancelable");
		}
	}
}
package events
{
	import flash.events.Event;
	
	import mx.events.FlexEvent;
	
	public class ShowBigPictureEvent extends FlexEvent
	{
		public static const DEFAULT_NAME:String = "de.hausarbeit.events.ShowBigPictureEvenet";
		public static const ON_SHOW_BIG:String = "onShowBigPicture";
		public var key:String;

		public function ShowBigPictureEvent(_type:String, _key:String, _bubbles:Boolean=false, _cancelable:Boolean=false)
		{
			super(_type, _bubbles, _cancelable);
			key = _key;
		}
		
		public override function clone() : Event
		{
			return new ShowBigPictureEvent(type, this.key, bubbles, cancelable);
		}
		
		public override function toString(): String
		{
			return formatToString("ShowBigPictureEvent", "key", "type", "bubbles", "cancelable");
		}
	}
}
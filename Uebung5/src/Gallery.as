package 
{
	import flash.display.*;
	import flash.events.*;
	import flash.net.*;
	
	import fp.lib.display.Utils;

	[SWF(width="720", height="640", frameRate="60", backgroundColor="#000000")]
	public class Gallery extends Sprite
	{		
		private const URL:String = "http://gamepc06.fh-trier.de:8080/fp/gallery/index.xml";
		
		private var galleryData:XML;
		private var imageCount:uint = 0;
		private var page:uint = 0;
		private var lastPage:uint = 0;
		private var currentImage:Object = null;
		
		public function Gallery()
		{			
			var file:URLLoader = new URLLoader();
			file.addEventListener(Event.COMPLETE, onLoadXml);
			file.load(new URLRequest(URL));
		}
		
		private function showControls(): void
		{
			var leftButton:Loader = new Loader();
			leftButton.addEventListener(MouseEvent.CLICK, onClickLeftButton);
			leftButton.contentLoaderInfo.addEventListener(Event.COMPLETE, showLeftButton);
			leftButton.load(new URLRequest("assets/Arrow.swf"));	
			
			var rightButton:Loader = new Loader();
			rightButton.addEventListener(MouseEvent.CLICK, onClickRightButton);
			rightButton.contentLoaderInfo.addEventListener(Event.COMPLETE, showRightButton);
			rightButton.load(new URLRequest("assets/Arrow.swf"));
		}
		
		private function showLeftButton(event:Event): void
		{
			var loader:Loader = event.target.loader as Loader;
			addChild(loader);
			loader.x = loader.width / 2;
			loader.y = stage.stageHeight - loader.height / 2;
			loader.rotation = 180;
			loader.visible = page > 0; 
			
			loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, showLeftButton);
		}
		
		private function showRightButton(event:Event): void
		{	
			var loader:Loader = event.target.loader as Loader;
			addChild(loader);
			loader.x = stage.stageWidth - loader.width / 2;
			loader.y = stage.stageHeight - loader.height / 2;
			loader.visible = page < lastPage;
			
			loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, showRightButton);
		}
		
		private function onClickLeftButton(event:MouseEvent): void
		{
			if (page > 0)
			{
				page--;
				loadPage();
			}
		}
		
		private function onClickRightButton(event:MouseEvent): void
		{
			//Utils.traceChildren(stage, 0);
			if (page < lastPage)
			{
				page++;
				loadPage();
			}
		}
			
		private function onLoadXml(event:Event): void
		{
			event.target.removeEventListener(Event.COMPLETE, onLoadXml);
			
			galleryData = new XML(event.target.data);
			// store number of pages
			lastPage = Math.floor(galleryData.images.image.length() / 16);
			loadPage();
		}
		
		private function loadPage(): void
		{
			// remove all objects
			imageCount = 0;
			while (numChildren > 0)
			{
				removeChildAt(0);
			}
			
			// parse xml that represents images of the current page
			for (var i:int = 0; i < 16; i++)
			{
				if (galleryData.images.image[i + page * 16] != null)
				{	
					loadImage(galleryData.images.image[i + page * 16].path);
				}
			}
			
			showControls();
		}

		private function loadImage(path:String): void
		{			
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadImageComplete);
			loader.load(new URLRequest(path));
		}
		
		private function onLoadImageComplete(event:Event): void
		{				
			var loader:Loader = event.target.loader as Loader;
			loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoadImageComplete);
			
			// scale each thumbnail image to a maximum width of 175
			loader.scaleX = loader.scaleY = 175 / loader.width;
			
			// set thumbnail position in order to get four pictures per row
			loader.x = (loader.width + 5) * (imageCount % 4);
			loader.y = 25 + (loader.height + 5) * Math.floor(imageCount / 4);
			
			loader.addEventListener(MouseEvent.CLICK, onClickImage);
			addChild(loader);
			
			imageCount++;
		}
		
		private function onClickImage(event:MouseEvent): void
		{
			var image:DisplayObject = event.target as DisplayObject;
			
			if (currentImage != null && currentImage.image == image)	// click away an image
			{
				// restore old status
				image.scaleX = image.scaleY = currentImage.scale;
				image.x = currentImage.x;
				image.y = currentImage.y;				
				currentImage = null;
				
				lighten();
			}
			else if (currentImage == null)								// no image selected yet
			{
				// store old status
				currentImage = {image:image, scale:image.scaleX, x:image.x, y:image.y};
				
				// center image
				image.scaleX = image.scaleY = 1;
				image.x = (stage.stageWidth - image.width) / 2;
				image.y = (stage.stageHeight - image.height) / 2;
				
				darken();
				
				// put on top
				addChild(image);
			}
		}
	
		private function darken(): void
		{
			// lower alpha value of all dispay object items except the current image 
			for (var i:int = 0; i < numChildren; i++)
			{
				var image:DisplayObject = getChildAt(i);
				if (image != currentImage.image)
				{
					image.alpha = 0.3;
				}
			}
		}
		
		private function lighten(): void
		{
			// reset all items' alpha value
			for (var i:int = 0; i < numChildren; i++)
			{
				getChildAt(i).alpha = 1.0;
			}
		}
	}
}
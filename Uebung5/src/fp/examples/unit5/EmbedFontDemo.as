package fp.examples.unit5
{
	import flash.text.*;
	
	import fp.EntryPoint;
	import fp.lib.text.Text;
	
	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class EmbedFontDemo extends EntryPoint
	{		
		[Embed(source="fp/examples/unit5/UbuntuRegular.swf", fontFamily="Ubuntu" )]
		private static var UbuntuRegular:Class;
		
		public function EmbedFontDemo()
		{	
			var format:TextFormat = new TextFormat();
			format.font = "Ubuntu";
			format.size = 16;
			format.color = 0xfff00;
			
			console.embedFonts = true;
			console.defaultTextFormat = format;
			
			console.print(Text.LOREM_IPSUM);
		}
	}
}
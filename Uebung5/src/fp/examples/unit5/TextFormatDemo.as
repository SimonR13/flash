package fp.examples.unit5
{
	import flash.text.*;
	
	import fp.EntryPoint;
	import fp.lib.text.Text;

	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class TextFormatDemo extends EntryPoint
	{
		public function TextFormatDemo()
		{
			var format:TextFormat = new TextFormat();
			format.align = TextFormatAlign.LEFT;
			format.font = "Helvetica Neue";
			format.bold = true;
			format.color = 0xffff00;
			
			for (var i:uint=0; i<20; i++)
			{
				format.size = 6 + (i*2);
				console.defaultTextFormat = format;
				console.print(Text.LOREM_IPSUM.substr(0, 30));
			}
		}
	}
}
package fp.examples.unit5
{
	import flash.display.Sprite;
	import flash.events.IOErrorEvent;
	import flash.media.*;
	import flash.net.URLRequest;
	
	public class SoundStreamDemo extends Sprite
	{		
		public function SoundStreamDemo()
		{			
			var sound:Sound = new Sound();
			var context:SoundLoaderContext = new SoundLoaderContext(12000);
			sound.addEventListener(IOErrorEvent.IO_ERROR, onError);
			sound.load(new URLRequest("http://www.tonycuffe.com/mp3/cairnomount.mp3"), context);
			sound.play();
		}
		
		private function onError(event:IOErrorEvent): void
		{
			trace("Fehler:", event.text);	
		}
	}
}
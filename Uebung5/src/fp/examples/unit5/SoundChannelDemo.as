package fp.examples.unit5
{
	import flash.display.Sprite;
	import flash.events.*;
	import flash.media.*;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	public class SoundChannelDemo extends Sprite
	{		
		private var sound:Sound;
		private var channel:SoundChannel;
		private var position:Number = -1;
		
		public function SoundChannelDemo()
		{			
			sound = new Sound();
			sound.addEventListener(IOErrorEvent.IO_ERROR, onError);
			sound.addEventListener(Event.COMPLETE, onComplete);
			sound.load(new URLRequest("assets/bwv565.mp3"));
		}
		
		private function onError(event:IOErrorEvent): void
		{
			trace("Fehler:", event.text);	
		}
		
		private function onComplete(event:Event): void
		{
			event.target.removeEventListener(Event.COMPLETE, onComplete);
			channel = event.target.play();
			
			var timer:Timer = new Timer(3000);
			timer.addEventListener(TimerEvent.TIMER, onTimer);
			timer.start();
		}
		
		public function onTimer(event:TimerEvent): void
		{
			if (position < 0)
			{
				position = channel.position;
				channel.stop();
			}
			else
			{
				trace(position);
				channel = sound.play(position);
				position = -1;
			}
		}
	}
}
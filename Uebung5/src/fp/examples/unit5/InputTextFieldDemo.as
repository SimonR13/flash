package fp.examples.unit5
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.*;
	
	public class InputTextFieldDemo extends Sprite
	{
		private var nameInput:TextField;
		private var passwordInput:TextField;
		private var output:TextField;
		
		public function InputTextFieldDemo()
		{
			var title:TextField = addTextField(10, 10, 100, 16, false);
			title.text = "Anmeldung";
			
			var nameLabel:TextField = addTextField(10, 40, 60, 16, false);
			nameLabel.text = "Name:";
			
			nameInput = addTextField(75, 40, 100, 16, true);
			nameInput.restrict = "a-z A-Z 0-9";
			nameInput.addEventListener(Event.CHANGE, onChange);
			
			var passwordLabel:TextField = addTextField(10, 70, 60, 16, false);
			passwordLabel.text = "Password:";
			
			passwordInput = addTextField(75, 70, 100, 16, true);
			passwordInput.displayAsPassword = true;
			passwordInput.addEventListener(Event.CHANGE, onChange);
			
			output = addTextField(10, 100, 165, 16, false);
			output.border = true;
		}
		
		private function addTextField(x:Number, y:Number, width:Number, height:Number, input:Boolean): TextField
		{
			var result:TextField = new TextField();
			result.x = x;
			result.y = y;
			result.width = width;
			result.height = height;
			
			result.selectable = input;
			result.border = input;
			if (input)
			{
				result.type = TextFieldType.INPUT;
			}
			
			addChild(result);
			return result;
		}
		
		private function onChange(event: Event): void
		{
			output.text = nameInput.text + "/" + passwordInput.text; 
		}
	}
}
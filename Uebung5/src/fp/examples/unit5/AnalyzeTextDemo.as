package fp.examples.unit5
{
	import flash.display.*;
	import flash.events.*;
	import flash.net.*;
	import flash.ui.Mouse;
	
	import fp.lib.text.*;

	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class AnalyzeTextDemo extends Sprite
	{
		private var output:Console;
		private var info:Console;
		
		public function AnalyzeTextDemo()
		{			
			output = new Console(this, 800, 500);
			output.print(Text.LOREM_IPSUM);
			output.print("\n");
			output.print(Text.LOREM_IPSUM);
			output.print("\n");
			output.print(Text.LOREM_IPSUM);
			
			info = new Console(this, 800, 100, 0xffffcc);
			info.y = output.height;
			
			addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}
		
		private function onMouseMove(event:MouseEvent): void
		{
			info.clear();
			
			var lineIndex:int = output.getLineIndexAtPoint(output.mouseX, output.mouseY);
			if (lineIndex >-1)
			{
				info.print(output.getLineText(lineIndex));
				info.print("");
				info.print("Zeilennummer: ", lineIndex);
				info.print("Zeilenbreite: ", output.getLineMetrics(lineIndex).width);
				info.print("Zeilenhöhe: ", output.getLineMetrics(lineIndex).height);
				info.print("Zeichenanzahl in Zeile: ", output.getLineLength(lineIndex));
			}
			
			var charIndex:int = output.getCharIndexAtPoint(output.mouseX, output.mouseY);
			if (charIndex > -1)
			{
				info.print("Aktuelles Zeichen: ", output.text.charAt(charIndex));	
			}
		}
	}
}
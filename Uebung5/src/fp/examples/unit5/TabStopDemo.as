package fp.examples.unit5
{
	import flash.text.*;
	
	import fp.EntryPoint;

	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class TabStopDemo extends EntryPoint
	{
		public function TabStopDemo()
		{
			var format:TextFormat = new TextFormat();
			format.tabStops = [30, 120];
			console.defaultTextFormat = format;
			
			console.print("Nr.\t| Wert 1\t| Wert2");
			console.print("------------------------------");
			for (var i:uint=0; i<40; i++)
			{				
				console.print(i+1 + "\t| " + Math.random().toFixed(5) + "\t| " + Math.random().toFixed(5)); 
			}
		}
	}
}
package fp.examples.unit5
{
	import flash.display.Sprite;
	import flash.text.*;
	
	public class WordWrapDemo extends Sprite
	{
		public function WordWrapDemo()
		{
			var noWrap:TextField = addTextField(10, 10, 100, 100);
			noWrap.wordWrap = false;
			noWrap.text = "(wordWrap = false):\nThis is very long text that will certainly extend beyond the width of this text field";
			
			var wrap1:TextField = addTextField(120, 10, 100, 100);
			wrap1.wordWrap = true;
			wrap1.text = "(wordWrap = true):\nThis is very long text that will certainly extend beyond the width of this text field";
			
			var wrap2:TextField = addTextField(230, 10, 100, 100);
			wrap2.wordWrap = true;
			wrap2.autoSize = TextFieldAutoSize.NONE;
			wrap2.text = "(wordWrap = true, autoSize = TextFieldAutoSize.NONE):\nThis is very long text that will certainly extend beyond the width of this text field and even beyond the height of this text field";
			
			var wrap3:TextField = addTextField(340, 10, 100, 100);
			wrap3.wordWrap = true;
			wrap3.autoSize = TextFieldAutoSize.LEFT;
			wrap3.text = "(wordWrap = true, autoSize = TextFieldAutoSize.LEFT):\nThis is very long text that will certainly extend beyond the width of this text field and even beyond the height of this text field";
		}
		
		private function addTextField(x:Number, y:Number, width:Number, height:Number): TextField
		{
			var result:TextField = new TextField();
			result.x = x;
			result.y = y;
			result.width = width;
			result.height = height;
			result.border = true;
			result.borderColor = 0xff0000;
			result.background = true;
			result.backgroundColor = 0xffffcc;
			result.multiline = true;
			addChild(result);
			return result;
		}
	}
}
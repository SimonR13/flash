package fp.examples.unit5
{
	import flash.events.TextEvent;
	import flash.text.*;
	
	import fp.EntryPoint;

	[SWF(width="800", height="600", frameRate="60", backgroundColor="#000000")]
	public class HTMLLinkDemo extends EntryPoint
	{
		public function HTMLLinkDemo()
		{
			var links:TextField = new TextField();
			addChild(links);
			links.x = 600;
			links.y = 50;
			links.height = 100;
			links.width = 200;
			links.background = true;
			links.backgroundColor = 0xffffff;
			links.defaultTextFormat = new TextFormat("Consolas", 16, 0x000000);
			links.selectable = false;
			links.multiline = links.wordWrap = true;
			
			links.addEventListener(TextEvent.LINK, onLink);
			links.htmlText = 	"<a href='http://www.fh-trier.de' target='_blank'>Besuche fh-trier.de</a><br/><br/>" +
								"<a href='event:random'>Erzeuge Zufallszahl</a><br/><br/>" +
								"<a href='event:now'>Zeige Zeitstempel an</a><br/><br/>";	
		}
		
		private function onLink(event:TextEvent): void
		{
			if (event.text == "random")
			{
				console.print(Math.random());
			}
			else if (event.text == "now")
			{
				console.print(new Date());
			}
		}
	}
}
package fp.examples.unit5
{
	import flash.events.*;
	import flash.media.*;
	import flash.net.URLRequest;
	import flash.ui.Keyboard;
	
	import fp.EntryPoint;
	
	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class SoundTransformDemo1 extends EntryPoint
	{
		public function SoundTransformDemo1()
		{			
			var sound:Sound = new Sound();
			sound.addEventListener(IOErrorEvent.IO_ERROR, onError);
			sound.addEventListener(Event.COMPLETE, onComplete);
			sound.load(new URLRequest("assets/bwv565.mp3"));
		}
		
		private function onError(event:IOErrorEvent): void
		{
			console.print("Fehler:", event.text);	
		}
		
		private function onComplete(event:Event): void
		{
			event.target.removeEventListener(Event.COMPLETE, onComplete);
			event.target.play();
			
			addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		}
		
		private function onKeyUp(event:KeyboardEvent): void
		{
			if (event.keyCode == Keyboard.SPACE)
			{
				var st:SoundTransform = new SoundTransform();
				if (SoundMixer.soundTransform.volume > 0)
				{
					st.volume = 0;
					console.print("Mute");
				}
				else
				{
					st.volume = 1;
					console.clear();
				}
				SoundMixer.soundTransform = st;
			}
		}
	}
}
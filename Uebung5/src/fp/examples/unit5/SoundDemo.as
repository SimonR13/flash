package fp.examples.unit5
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.media.Sound;
	import flash.net.URLRequest;
	
	import fp.EntryPoint;
	
	[SWF(width="800", height="600", frameRate="60", backgroundColor="#ffffff")]
	public class SoundDemo extends EntryPoint
	{		
		public function SoundDemo()
		{
			var sound:Sound = new Sound();
			sound.addEventListener(IOErrorEvent.IO_ERROR, onError);
			sound.addEventListener(ProgressEvent.PROGRESS, onProgress);
			sound.addEventListener(Event.COMPLETE, onComplete);
			sound.load(new URLRequest("assets/siren.mp3"));
		}
		
		private function onError(event:IOErrorEvent): void
		{
			console.print("Fehler:", event.text);	
		}
		
		private function onProgress(event:ProgressEvent): void
		{
			console.print("Fortschritt: " + Math.round((event.bytesLoaded / event.bytesTotal) * 100) + "%");
		}
		
		private function onComplete(event:Event): void
		{
			event.target.removeEventListener(Event.COMPLETE, onComplete);
			event.target.play();
		}
	}
}
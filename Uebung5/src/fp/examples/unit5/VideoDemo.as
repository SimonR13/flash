package fp.examples.unit5
{
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Rectangle;
	import flash.media.*;
	import flash.net.*;
	import flash.ui.*;
	
	public class VideoDemo extends Sprite
	{
		private var video:Video;
		
		public function VideoDemo()
		{			
			var connection:NetConnection = new NetConnection();
			connection.connect(null);
			
			var stream:NetStream = new NetStream(connection);
			
			var metaDataHandler:Object = new Object();
			metaDataHandler.onMetaData = onMetaData;
			stream.client = metaDataHandler;
			
			video = new Video();
			video.attachNetStream(stream);
			addChild(video);
			
			stream.play("assets/theforce.flv");
			
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		}
		
		private function onMetaData(info:Object): void
		{
			video.width = info.width/ 2;
			video.height = info.height / 2;
		} 
		
		private function onKeyUp(event:KeyboardEvent): void
		{
			if (event.keyCode == Keyboard.SPACE && stage.displayState == StageDisplayState.NORMAL)
			{
				stage.fullScreenSourceRect = new Rectangle(video.x, video.y, video.width, video.height);
				stage.displayState = StageDisplayState.FULL_SCREEN;
			}
		}
	}
}
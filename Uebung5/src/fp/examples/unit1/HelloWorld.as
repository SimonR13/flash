package fp.examples.unit1
{
	import flash.display.Sprite;

	[SWF(width="200", height="200", frameRate="60", backgroundColor="#000000")]
	public class HelloWorld extends Sprite
	{
		public function HelloWorld()
		{
			trace("Hello World!");
		}
	}
}
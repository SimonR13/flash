package fp.lib.display
{
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	
	import fp.lib.text.Text;

	public final class Utils
	{
		public static function traceChildren(container:DisplayObjectContainer, recursionLevel:uint): void
		{
			for (var i:uint = 0; i < container.numChildren; i++)
			{
				var displayObject:DisplayObject = container.getChildAt(i);
				trace(Text.repeatString(" ", recursionLevel), displayObject);
				if (displayObject is DisplayObjectContainer)
				{
					traceChildren(displayObject as DisplayObjectContainer, recursionLevel+1);
				}
			}
		}
		
		public function Utils()
		{
			throw new ArgumentError("Utils is a static utility class.");
		}
	}
}
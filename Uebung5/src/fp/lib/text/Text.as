package fp.lib.text
{
	public final class Text
	{
		public static const LOREM_IPSUM: String = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";
		
		public static function repeatString(str:String,repeatCount:uint): String
		{
			var result:String = "";
			for (var i:uint = 0; i < repeatCount; i++)
			{
				result += str;
			}
			return result;
		}
		
		public function Text()
		{
			throw new ArgumentError("Text is a static utility class.");
		}
	}
}